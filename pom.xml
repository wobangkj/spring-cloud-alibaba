<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <artifactId>spring-cloud-alibaba</artifactId>
    <groupId>com.wobangkj</groupId>
    <packaging>pom</packaging>
    <version>0.0.2</version>

    <modules>
        <module>common-service</module>
        <module>auth-service</module>
        <module>api-gateway</module>
        <module>shop-service</module>
    </modules>

    <!-- 使用spring-boot-starter-parent管理jar包版本 -->
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.1.5.RELEASE</version>
    </parent>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>
        <spring-cloud.version>Greenwich.SR1</spring-cloud.version>
        <alibaba.version>0.9.0.RELEASE</alibaba.version>

        <!-- 自定义配置 -->
        <!-- docker (私有)仓库 -->
        <dockerRepository>registry.cn-hangzhou.aliyuncs.com/wobangkj/dreamlu</dockerRepository>

        <!-- 开发环境 -->
        <jdbc.url.dev>
            <![CDATA[jdbc:mysql://localhost:3306/spring-cloud?useUnicode=true&characterEncoding=UTF-8&sessionVariables=FOREIGN_KEY_CHECKS=0&autoReconnect=true]]></jdbc.url.dev>
        <jdbc.username.dev>root</jdbc.username.dev>
        <jdbc.password.dev>lucheng</jdbc.password.dev>
        <!-- 服务发现 -->
        <nacos.url.dev>127.0.0.1:8848</nacos.url.dev>
        <sentinel.dashboard.dev>localhost:8088</sentinel.dashboard.dev>
        <!-- redis url -->
        <redis.url.dev>localhost</redis.url.dev>

        <!-- docker/prod环境 -->
        <jdbc.url.prod>
            <![CDATA[jdbc:mysql://mysql:3306/spring-cloud?useUnicode=true&characterEncoding=UTF-8&sessionVariables=FOREIGN_KEY_CHECKS=0&autoReconnect=true]]></jdbc.url.prod>
        <jdbc.username.prod>root</jdbc.username.prod>
        <jdbc.password.prod>dreamlu</jdbc.password.prod>
        <!-- 服务发现 -->
        <nacos.url.prod>nacos:8888</nacos.url.prod>
        <sentinel.dashboard.prod>sentinel:8088</sentinel.dashboard.prod>
        <!-- redis url -->
        <redis.url.prod>redis</redis.url.prod>

        <!-- 单个文件上传大小,单位MB -->
        <max-file-size>32</max-file-size>
        <!-- 一次文件上传最大大小 -->
        <max-request-size>320</max-request-size>

        <!-- 开发模式, dev/prod -->
        <devMode>prod</devMode>

    </properties>


    <dependencies>
        <!-- 工具类 -->
        <dependency>
            <artifactId>java-tool</artifactId>
            <groupId>com.wobangkj</groupId>
            <version>0.0.4</version>
        </dependency>

        <!--    重构    -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
            <version>${alibaba.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
            <version>${alibaba.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
            <version>${alibaba.version}</version>
        </dependency>

        <dependency>
            <groupId>com.alibaba.csp</groupId>
            <artifactId>sentinel-datasource-nacos</artifactId>
            <version>1.6.0</version>
        </dependency>

        <!-- Compile -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <!-- 使用jetty代替tomcat, get请求字符问题 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-jetty</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-config</artifactId>
        </dependency>
        <!-- 配置相关依赖 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
        </dependency>

        <!--JVM reloading agent-->
        <!-- 热部署, dubbo 冲突 -->
<!--        <dependency>-->
<!--            <groupId>org.springframework.boot</groupId>-->
<!--            <artifactId>spring-boot-devtools</artifactId>-->
<!--        </dependency>-->

        <!-- Lombok -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.6</version>
            <scope>provided</scope>
        </dependency>

        <!-- json 解析 -->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.58</version>
        </dependency>

    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <!--fork:如果没有该项配置,整个devtools不会起作用, 热部署相关-->
                    <fork>true</fork>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <useSystemClassLoader>false</useSystemClassLoader>
                </configuration>
            </plugin>
        </plugins>

    </build>

    <profiles>
        <profile>
            <id>java1.8</id>
            <activation>
                <jdk>1.8</jdk>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <configuration>
                            <skipTests>true</skipTests>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <!-- 阿里私有仓库地址 -->
        <profile>
            <id>rdc-private-repo</id>
            <repositories>
                <repository>
                    <id>rdc-releases</id>
                    <url>https://repo.rdc.aliyun.com/repository/79115-release-8qAEIB/</url>
                </repository>
                <repository>
                    <id>rdc-snapshots</id>
                    <url>https://repo.rdc.aliyun.com/repository/79115-snapshot-nphODc/</url>
                </repository>
            </repositories>
        </profile>
    </profiles>

    <!-- 仓库地址源更换-->
    <repositories>
        <repository>
            <id>aliyun-repos</id>
            <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
    </repositories>

    <pluginRepositories>
        <pluginRepository>
            <id>aliyun-plugin</id>
            <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>
</project>
