package com.wobangkj.common.model.file;

import lombok.Data;

/**
 * @author dreamlu
 * @date 2019/07/06
 */
@Data
public class FileUri {
	// uri
	private String uri;

	// file name
	private String name;

	// file type
	private String type;
}
