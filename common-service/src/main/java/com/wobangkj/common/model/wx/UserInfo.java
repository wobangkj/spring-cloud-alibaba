package com.wobangkj.common.model.wx;

import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dreamlu
 * @date 2019/07/10
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
public class UserInfo extends WxMaUserInfo {

	// 手机号(国外包含区号)
	private String phone;

	public UserInfo(WxMaUserInfo userInfo, String phoneNumber) {
		this.phone = phoneNumber;
		this.setAvatarUrl(userInfo.getAvatarUrl());
		this.setCity(userInfo.getCity());
		this.setWatermark(userInfo.getWatermark());
		this.setUnionId(userInfo.getUnionId());
		this.setProvince(userInfo.getProvince());
		this.setOpenId(userInfo.getOpenId());
		this.setNickName(userInfo.getNickName());
		this.setLanguage(userInfo.getLanguage());
		this.setGender(userInfo.getGender());
		this.setCountry(userInfo.getCountry());
	}
}
