package com.wobangkj.common.model.pca;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author dreamlu
 * @date 2019/06/14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Area {

	private String code;

	private String name;
}
