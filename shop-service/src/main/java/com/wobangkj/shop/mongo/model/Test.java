package com.wobangkj.shop.mongo.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

/**
 * @author dreamlu
 * @date 2019/06/24
 */
@Document
@Data
public class Test {

	@Id
	private String id;

	private String name;

	private String phone;
}
