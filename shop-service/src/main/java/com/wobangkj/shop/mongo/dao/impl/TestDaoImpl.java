//package com.wobangkj.shop.mongo.dao.impl;
//
//import com.wobangkj.shop.mongo.dao.TestDao;
//import com.wobangkj.shop.mongo.model.Test;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.query.Criteria;
//import org.springframework.data.mongodb.core.query.Query;
//import org.springframework.data.mongodb.core.query.Update;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.lang.reflect.Field;
//
//@Component
//public class TestDaoImpl implements TestDao {
//
//	@Resource
//	private MongoTemplate mongoTemplate;
//
//	@Override
//	public void create(Test data) {
//		mongoTemplate.save(data);
//	}
//
//	@Override
//	public void delete(String id) {
//		Query query = new Query(Criteria.where("id").is(id));
//		mongoTemplate.remove(query, Test.class);
//	}
//
//	@Override
//	public void update(Test data) {
//		Query query = new Query(Criteria.where("id").is(data.getId()));
//
//		Update  update = new Update();
//		Field[] fields = data.getClass().getDeclaredFields();
//		for (Field field : fields) {
//			//设置对象的访问权限，保证对private的属性的访问
//			field.setAccessible(true);
//			try {
//				if (field.get(data) == null) {
//					continue;
//				}
//				update.set(field.getName(), field.get(data));
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//
//		mongoTemplate.updateMulti(query, update, Test.class);
//	}
//
//	@Override
//	public Test id(String id) {
//		Query query = new Query(Criteria.where("id").is(id));
//		Test  test  = mongoTemplate.findOne(query, Test.class);
//		return test;
//	}
//
////	@Override
////	public List<Test> search(Map<String, Object> params) {
////		Query query = new Query();
////
////		query.addCriteria(Criteria.byExample());
////		List<Test>  list  = mongoTemplate.find(query, Test.class);
////		return list;
////	}
//
//}