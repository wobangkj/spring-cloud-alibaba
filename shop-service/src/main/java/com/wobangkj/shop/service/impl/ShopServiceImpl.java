package com.wobangkj.shop.service.impl;

import com.wobangkj.shop.service.common.commonService;
import com.wobangkj.tool.api.result.Result;
import com.wobangkj.shop.dao.ShopDao;
import com.wobangkj.shop.model.Shop;
import com.wobangkj.shop.model.ShopDe;
import com.wobangkj.shop.service.ShopService;
import com.github.pagehelper.PageHelper;
import com.wobangkj.shop.repository.ShopRepository;
import com.wobangkj.tool.manager.cache.CacheManager;
import com.wobangkj.tool.manager.cache.impl.RedisManager;
import com.wobangkj.tool.model.CacheModel;
import com.wobangkj.tool.model.TokenModel;
import com.wobangkj.tool.model.wx.ModelMsgReturn;
import com.wobangkj.tool.util.sql.CrudUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.connection.RedisConnectionFactory;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.wobangkj.tool.lib.Constants.TOKEN_MINUTE;
import static com.wobangkj.tool.util.RestUtil.copyNonNullProperties;

@Slf4j
//@org.springframework.stereotype.Service
@Service(protocol = "dubbo")
public class ShopServiceImpl implements ShopService {

	@Autowired
	private ShopRepository repository;

	@Autowired
	private commonService commonService;

	@Override
	public Object search(Map<String, Object> params, Shop data) {

		return CrudUtil.search(params, data, repository);
	}

	public Shop id(Long id) {

		return repository.findById(id).orElse(null);
	}

	@Override
	public Object idDe(Long id) {
		List<ShopDe> list = shopDao.IdDe(id);

		if (list.size() == 0) {
			return Result.MapNoResult;
		}

		return Result.GetMapDataSuccess(list);
	}

	@Override
	public Object delete(Long id) {
		return CrudUtil.delete(id, repository);
	}

	@Autowired
	private ShopDao shopDao;

	// 用jpa 写sql
	@Override
	public Object searchDe(Map<String, Object> params, ShopDe data) {

		// TODO mybatis 方式查询
		Integer clientPage = Integer.parseInt((String) params.get("clientPage"));
		Integer everyPage  = Integer.parseInt((String) params.get("everyPage"));

		com.github.pagehelper.Page<ShopDe> page = PageHelper.startPage(clientPage, everyPage);

		List<ShopDe> list = shopDao.searchDe(params);

		if (list.size() == 0) {
			return Result.MapNoResult;
		}

		return Result.GetMapDataPager(list, clientPage, (int) page.getTotal(), everyPage);
	}

	@Override
	public Object update(Shop shop) {

		Shop existData = repository.findById(shop.getId()).get();
		copyNonNullProperties(shop, existData);
		return CrudUtil.update(existData, repository);
	}

	@Override
	@SuppressWarnings("Duplicates")
	public Object create(Shop shop) {
		try {
			Shop createData = repository.save(shop);
			// 使用 uuid 作为源 token
			String     token = UUID.randomUUID().toString().replace("-", "");
			TokenModel model = new TokenModel(createData.getId(), token);
			// 30 分钟有效期
			cacheManager.set(model.getToken(), new CacheModel(TOKEN_MINUTE, model));
			return Result.GetMapData(Result.CodeCreate, Result.MsgCreate, model);
		} catch (Exception e) {
			return Result.GetMapData(Result.CodeSql, e.getCause().getCause().getMessage());
		}
	}

	private CacheManager cacheManager;

	@Autowired
	public void setRedis(RedisConnectionFactory redisConnectionFactory) {
		this.cacheManager = new RedisManager(redisConnectionFactory);
	}

	@Override
	@SuppressWarnings("Duplicates")
	public Object getIdByUnionid(String unionid) {

		List list = repository.getIdByUnionid(unionid);
		if (list.size() == 0) {
			return Result.MapNoResult;
		}
		Long id = (long) list.get(0);
		// 使用 uuid 作为源 token
		String     token = UUID.randomUUID().toString().replace("-", "");
		TokenModel model = new TokenModel(id, token);
		// 30 分钟有效期
		cacheManager.set(model.getToken(), new CacheModel(TOKEN_MINUTE, model));

		return Result.GetMapData(Result.CodeSuccess, Result.MsgSuccess, model);
	}

	@Override
	public Object getShopByName(String name, Integer clientPage, Integer everyPage) {
		Page<Shop> page = repository.findByName(name, PageRequest.of(clientPage - 1, everyPage, Sort.by("id")));
		log.info("[jpa原生sql查询 shop列表:]" + page.getContent());
		return Result.GetMapDataSuccess(page.getContent());
	}
}
