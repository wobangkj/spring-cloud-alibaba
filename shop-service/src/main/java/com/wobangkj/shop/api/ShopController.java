package com.wobangkj.shop.api;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.wobangkj.shop.model.ShopDe;
import com.wobangkj.shop.service.ShopService;
import com.wobangkj.shop.model.Shop;
import com.wobangkj.shop.service.common.commonService;
import com.wobangkj.tool.model.wx.ModelMsgReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/shop")
public class ShopController {

	@Autowired
	private ShopService shopService;

	@PostMapping(value = "/create")
	public Object create(@RequestBody Shop data) {
		// 创建时间
		data.setCreatetime(Timestamp.valueOf(LocalDateTime.now()));
		return shopService.create(data);
	}

	@PutMapping(value = "/update")
	public Object update(@RequestBody Shop shop) {

		return shopService.update(shop);
	}

	@DeleteMapping(value = "/delete/{id}")
	public Object delete(@PathVariable(value = "id") Long id) {
		return shopService.delete(id);
	}

	@SentinelResource(value = "protected-resource", blockHandler = "handleBlock")
	@GetMapping("/id")
	public Shop id(Long id) {

		log.info("[sentinel测试]： 不限流");
		return shopService.id(id);
	}

	public Object handleBlock(Long id, BlockException e) {
		log.error("[sentinel测试]： 限流");
		return new ModelMsgReturn(0, "限流");
	}


	@GetMapping("/search")
	public Object search(@RequestParam(required = false) Map<String, Object> params, Shop shop) {
		return shopService.search(params, shop);
	}

	@GetMapping("/idDe")
	public Object idDe(Long id) {

		return shopService.idDe(id);
	}

	@GetMapping("/searchDe")
	public Object searchDe(@RequestParam(required = false) Map<String, Object> params, ShopDe shop) {
		return shopService.searchDe(params, shop);
	}


	@GetMapping("/getId")
	public Object getIdByUnionid(String unionid) {
		return shopService.getIdByUnionid(unionid);
	}
}
